# etapa de compilación
FROM node:13.4.0-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

# etapa de producción
FROM nginx:1.17.6-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
COPY --from=build-stage /app/nginx.conf /usr/share/nginx/nginx.conf

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]