# quick-poll

## Installar proyecto
```
npm install
```

### Compilar en modo desarrollador
```
npm run serve
```

### compoilar para producción
```
npm run build
```

### Correr los test unitarios
```
npm run test:unit
```

### Correr los test e2e
```
npm run test:e2e
```

### Descripción rápida
Se desarrollo un proyecto web con el objetivo de solventar una necesidad de crear encuestas rápidas. La página inicial nos permite crear una poll o ingresar un codigo que nos hayan compartido previamente.

### Tecnologías utilizadas
##### - Vue 2.6
##### - Firebase 7.6

### Vue
Para crear el proyecto utilizamos **VueCLI 3** que ya incluye **vuex**  y  **router**. Vuex permite controlar variables globales y router nos permite cambiar de componentes o vistas.

### Firebase
Firebase es una plataforma para desarrollo web de la cual nosotros estamos utilizando el servicio de **_real time data base_** que nos permite poder mostrar datos en tiempo real sobre los cambios que hacemos en la base de datos.

### Configuración firebase
Para configurar firebase   en nuestro poyecto primero debemos poner nuestras credenciales del proyecto que ya hemos creado previamente en firebase que en este caso se llama quickpoll. Para acceder a estas credenciales ingresamos al proyecto de firebase y seleccionamos la opción *añadir proyecto*  ![](src/assets/add_project_fb.png)  si ya lo tiene creado solo lo seleccionas y abres sus configuraciones, y copiamos las credenciales que nos genera firebase ![](src/assets/credentials.png).

Luego en nuestro projecto vue pegamos nuestras credenciales en el archivo main.js y en ese mismo archivo declaramos que vue utilizará real database con `Vue.use(rtdbPlugin);` donde rtdbPlugin es una variable que tiene el import de **vuefire**.

### Proyecto vue
##### main.js
Cuando creamos nuestra proyecto con **VueCLI** nos crea muchas configuraciones base, y la que inicia el proyecto es *main.js*  pues es ahi donde creamos nuestra instancia de vue, donde le damos variables fijas que tendran todos nuestros componente vue, estas variables son: router y store. Debemos recordar que esas dos variables tienen el valor de los imports que le demos, que en el caso es: `import router from './router';` y en el caso de store es:  `import store from './store'`. También vemos que tenemos el render a al archivo **App**.

##### App.vue
```javascript
<template>
  <div id="app">
    <div id="nav">
      <router-link to="/">Home</router-link> |
      <router-link to="/about">About</router-link>
    </div>
    <router-view/>
  </div>
</template>
```
Podemos ver que tenemos un nav que estara presente en todos los componentes que llamemos por medio del **router**. `<router-view>` hace referencia al archivo **index.js** que esta en nuestra carpeta *router*.

##### Index.js(router)
```javascript
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
...

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  ...
]

const router = new VueRouter({
  routes
})

export default router
```
Como observamos tenemos estructura muy sencilla y fácil de entender, en los import llamamos a todos los componentes y vistas que vayamos a utilizar, aunque no devemos olvidar que es obligatorio tener `import Vue from 'vue'` y `import VueRouter from 'vue-router'` pues sin estos import no podemos utilizar el router. La variable *router* que estamos exportando al final es el mismo que tenemos en el *mail.js*, es por ello que por medio de esa variable podemos acceder a las vistas o componentes que tengamos declaradas en este archivo router.

Para añadir un componente o vista solo debemos importarlo como en el ejemplo de que tenemos, donde **Home** es el nombre y se dice de donde los estamos importando, en en su caso es de la carpeta *views* y la la vista *Home.vue*. Luego lo añadimos al array router, asignando **path** por la cual se puede acceder, **name** por el cual puede ser llamado por otros componentes  y el nombre del componente al que hacemos referencia, en este caso llamamos a la variable *Home*.

[Clic aquí](https://router.vuejs.org/guide/essentials/nested-routes.html) para más información.

##### index.js(store)
**Store** es el centro de cualquier aplicación **VUEX**.
```javascript
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    code:''
  },
  mutations: {
    addCode(state, enter_code){
      state.code=enter_code;
    }
  },
  actions: {
    setCode({commit},enter_code){
      commit('addCode', enter_code);
    }
  },
  modules: {
  },
  getters:{
    getCode: state =>{
      return state.code;
    }
  }
})
```
Store una clase que nos permite utilizar variables que son muy similares a las variables globales, pues son accesibles desde todos los componentes o vistas de vue. Estas variables se manejan por **_states_**, los cuales son sus valores, y la forma de cambiar los states de las variables son por **_mutations_**, de esta manera es entendible saber en donde se le cambia el valor a estas variables. También es recomendable utilizar los **_actions_**, que se encargan de llamar a los *mutations*, y aunque parezca un poco redundante es mucho más seguro. Para más información [clic aquí](https://vuex.vuejs.org/guide/state.html)

#### Flujo del proyecto
Como pudimos observar en el *mail.js* primero vamos a *App.vue*, en donde creamos el *nav* que tiene dos opciones, *Home* que nos redirige a */* que es la vista *Home.vue* y *About* que nos redirige a */about*  que es la vista *About.vue*. También en App.vue tenemos `<route-view>`. Por defecto cuando iniciemos el proyecto iniciaremos en *"dominio:/"* que por defecto nos habrira Home.vue. Para verficar esto podemos ver el **_Index.js(router)_**.

**Home.vue**
```javascript
<template>
  <div class="home">
    <div class="container">
      <EnterPoll/>
      <CreatePoll />
    </div>
  </div>
</template>
// @ is an alias to /src
import CreatePoll from '@/components/CreatePoll.vue'
import EnterPoll from '@/components/EnterPoll.vue'

export default {
  name: 'home',
  components: {
    CreatePoll,
    EnterPoll,
  }
}
```
Podemos ver que llamamos a dos componente, **_EnterPoll.vue_** y **_CreatePoll.vue_**. Estos componens no estan en el router pues no son necesarios asignarles una path a cada uno, pues nunca son llamados por separado.

### EnterPoll.vue
EnterPoll es el componente donde ingresamos un código y nos dirigimos a la vista *Voting.vue* para votar. Como debemos acceder a la base de datos de firebase tenemos que importarla la variable desde main.js que es donde la estamos exportando. Este es el comando para `import { RTDB } from "../main.js";` y para utilizarlo solo utilizamos la variable *RTDB*.

EnterPoll debe verificar si el código que estamos ingresando existe o no en la base de datos, pues si no existe no debe permitir que cargue el siguiente comonente. Para verificar el código tenemos la función **_navigator_** donde primero verificamos que no se haya ingresado un código, después con el array **key** que hemos cargado previamente en **_created()_** con toda la base de datos, verificamos que el código este en ella, si el código esta en la base de datos guardamos el código en el *store*, esto lo hacemos con `this.$store.dispatch('setCode',code);` donde llamamos al action *setCode*  y le pasamos el código, de esta manera podremos acceder al código desde el componente **Voting.vue** y **ViewPoll.vue**, pero si el código no se encuntra en el array mandamos una alerta indicando que el código es erroneo.

### CreatePoll.vue
CreatePoll se encarga de crear nuevas polls. De la misma forma debemso importar *RTDB* para ingresar las nueva poll, y al crearla nos genera un código que es el que se debe compartir para que los demas puedan votar. Para guardar la poll solo le damos dos datos, un string con la pregunta y un array con todas las opciones.
```javascript
RTDB.ref('polls/'+randomString).set({
        question:this.poll_question,
        answers: this.poll_options,
});
```
Debemos limpiar siempre las variables para que cuando creemos otra poll no se dupliquen datos o se guarden más opciones de las que el usuario ingresa por poll.

### Voting.vue
Primero cargamos la información en el array *options*  en *created()*. Para que vue pueda dibujar especificamente la poll que queremos leemos el store, especificamente un getter llamado *getCode*, para poder usarlo los creamos en **computed** de esta forma `...mapGetters(['getCode']),` donde mapeamos todos los getters. Para votar tenemos el método *edit(child,value)* donde *child* es la opción especifica que fue seleccionada y *value* el número de votos que tiene la opción.

Para correguir el voto tenemos el botón **Change Vote** que aparece solo cuando ya fue efectuado el voto, esto lo hacemos con `v-show="terms"` donde *terms* es una variable booleana que cambia de valor cuando llamamos al método *edit(child,value)*. *Change Vote*  llama al método *ChangeVote()* que lo que hace es restar un voto a la opción que se selecciono, en este caso ya no es necesario pasarle la opción seleccionada como parametro pues la guardamos en la variable *option_selected* en el método *edit(child, value)*. También cambiamos el valor de la variable **terms** que es la variable booleana que hace que se boqueen los botones para votar.

Por último tenemos el botón **Results** que nos redirige al componente **ViewPoll.vue**. Este botón llama al método *graph* que con ayuda al router hace un push a la otra opción que tiene guardados, que en este caso hace push al componente con el nombre *ViewPOll*.

### ViewPoll.vue
En ViewPoll mostramos las opciones y el número de votos que tiene cada opción. En *mounted()*  cargamos los datos directamente de *RTDB* en dos variables, **poll_quiestion** y **poll_option**, y llamamos al método *calculateTotalVotes(poll_options)* donde guardamos el número de votos en *total_votes*. Teniendo estos datos vue comienza a dibujar los componentes. Para mostrar el porcentaje llamamos al método *calculatePercent(votes, total_votes)* que devuelve el porcentaje de votos que tiene cada opción.
