import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    no_members:0,
    code_family:'',
    email:'',
    id:'2',
    code:'',
    is_registered:false
  },
  mutations: {
    addNoMembers(state, no_members){
      state.no_members=no_members;
    },
    addCode(state, enter_code){
      state.code=enter_code;
    },
    addCodeFamily(state, enter_code_family){
      state.code_family=enter_code_family;
    },
    addEmail(state, email){
      state.email=email;
    },
    addId(state,id){
      state.id=id;
    },
    login(state){
      state.is_registered = true;
    },
    logout(state){
      state.is_registered = true;
    }
  },
  actions: {
    setCode({commit},enter_code){
      commit('addCode', enter_code);
    },
    setNoMembers({commit},no_members){
      commit('addNoMembers', no_members);
    },
    setCodeFamily({commit},enter_code_family){
      commit('addCodeFamily', enter_code_family);
    },
    setEmail({commit},email){
      commit('addEmail',email);
    },
    setId({commit},id){
      commit('addId',id);
    },
    login({commit}){
      commit('login');
    },
    logut({commit}){
      commit('logout');
    }
  },
  modules: {
  },
  getters:{
    getCode: state =>{
      return state.code;
    },
    getNoMembers: state =>{
      return state.no_members;
    },
    getCodeFamily: state=>{
      return state.code_family;
    },
    getId:state=>{
      return state.id;
    },
    getState:state=>{
      return state.is_registered;
    },
    getEmail:state=>{
      return state.email;
    },
    getId:state=>{
      return state.id;
    }
  }
})
