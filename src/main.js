// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import * as firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';
import { rtdbPlugin } from 'vuefire';
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store'
import Toasted from 'vue-toasted';
  
Vue.use(rtdbPlugin);
Vue.use(Toasted);

const firebaseConfig = {
    apiKey: "AIzaSyBBvEVaT_LUfYSbNkMOgoRW0EQZJawH0PU",
    authDomain: "appartamento-2bbcf.firebaseapp.com",
    databaseURL: "https://appartamento-2bbcf.firebaseio.com",
    projectId: "appartamento-2bbcf",
    storageBucket: "appartamento-2bbcf.appspot.com",
    messagingSenderId: "672302474858",
    appId: "1:672302474858:web:9f1f4395dbb2ac34bf0c13",
    measurementId: "G-925FJQP7K7"
};  
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const RTDB = firebase.database();
export const AUTH = firebase.auth();

Vue.config.productionTip = false;

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
}).$mount();
