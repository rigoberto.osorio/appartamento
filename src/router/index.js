import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Expense from '@/views/Expense.vue'
import Decision from '@/views/Decision.vue'
import Rule from '@/views/Rule.vue'
import Fault from '@/views/Fault.vue'
import Voting from '@/components/Voting.vue'
import Login from '@/components/Login.vue'
import Register from '@/components/Register.vue'
import ViewPoll from '@/components/ViewPoll.vue'
import CreateDecision from '@/components/CreateDecision.vue'
import VoteDecision from '@/components/VoteDecision.vue'
import CreateExpense from '@/components/CreateExpense.vue'
import MainView from '@/components/Home.vue'
import CreateFault from '@/components/CreateFault.vue'
import ViewFault from '@/components/ViewFault.vue'
import Code from '@/views/Code.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'home',
    component: Home
  },
  {
    path:'/CreateExpense',
    name: 'createExpense',
    component:CreateExpense
  },
  {
    path:'/CreateDecision',
    name: 'createDecision',
    component:CreateDecision
  },

  {
    path:'/VoteDecision',
    name: 'VoteDecision',
    component:VoteDecision,
    props:true
  },
  {
    path: '/rule',
    name: 'rule',
    component: Rule
  },
  {
    path: '/',
    name: 'login',
    component: Login
  }, 
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/expense',
    name: 'expense',
    component: Expense
  },
  {
    path: '/fault',
    name: 'fault',
    component: Fault
  },
  {
    path: '/code',
    name: 'code',
    component: Code
  },
  {
    path: '/decision',
    name: 'decision',
    component: Decision
  },
  {
    path: '/createFault',
    name: 'CreateFault',
    component: CreateFault
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path:'/main',
    name:'main',
    component:MainView
  }
]

const router = new VueRouter({
  routes
})

export default router
